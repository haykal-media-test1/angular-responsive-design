import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { SubscriptionsGuard } from './subscriptions/guards/subscriptions.guard';

const ROUTES: Routes = [
  {
    path: '',
    loadChildren: () => import('./subscriptions/subscriptions.module').then(s => s.SubscriptionsModule),
    canLoad: [SubscriptionsGuard],
    canActivate: [SubscriptionsGuard]
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
