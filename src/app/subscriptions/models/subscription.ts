export class Subscription {
    constructor(public title: string, public description: string, public message: string, public fee: number, public currency: string, public per: string, public features: string[]) {
    }
}