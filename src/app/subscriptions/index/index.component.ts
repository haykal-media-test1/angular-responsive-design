import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from '../models/subscription';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  public subscriptions: Subscription[];

  constructor(route: ActivatedRoute) {
    route.data.subscribe(data => {

      this.subscriptions = data.items;
    });
  }

  ngOnInit(): void {
  }

}
