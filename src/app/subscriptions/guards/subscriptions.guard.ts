import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsGuard implements CanActivate, CanLoad {

  public canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    // Here we can checl if the user is authorised
    // to access subscriptions
    return true;
  }

  public canLoad(route: Route, segments: UrlSegment[]): boolean {
    // Here we can checl if the user is authenticated
    return true;
  }
}
