import { TestBed } from '@angular/core/testing';

import { SubscriptionsGuard } from './subscriptions.guard';

describe('SubscriptionsGuard', () => {
  let guard: SubscriptionsGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(SubscriptionsGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
