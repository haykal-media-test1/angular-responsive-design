import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Subscription } from './models/subscription';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionsResolver implements Resolve<Subscription[]> {

  constructor() { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Subscription[]> {

    const data: Subscription[] = [

      new Subscription("اشتراك سنوي",
        "اشتراك رقمي فقط",
        "أفضل قيمة | وفر 47 دولار",
        73,
        "$",
        "سنوياً",
        [
          "وصول رقمي للمحتوى عبر التطبيق والموقع",
          "30 إصدار من المجلة",
          "300 فيديو قيم يثري معارفك",
          "5 آلاف مقالة يومياً",
          "مكتبة المحتوى الصوتي",
          "نشرة بريدية خاصة بالأعضاء",
        ]),
      new Subscription(
        "اشتراك 6 أشهر",
        "اشتراك رقمي فقط",
        "وفر 28 دولار",
        46,
        "$",
        "كل 6 أشهر",
        [
          "وصول رقمي للمحتوى عبر التطبيق والموقع",
          "30 إصدار من المجلة",
          "300 فيديو قيم يثري معارفك",
          "5 آلاف مقالة يومياً",
          "مكتبة المحتوى الصوتي",
          "نشرة بريدية خاصة بالأعضاء",
        ],
      ),
      new Subscription(
        "اشتراك شهري",
        "اشتراك رقمي فقط",
        "",
        10,
        "$",
        "شهرياً",
        [
          "وصول رقمي للمحتوى عبر التطبيق والموقع",
          "30 إصدار من المجلة",
          "300 فيديو قيم يثري معارفك",
          "5 آلاف مقالة يومياً",
          "مكتبة المحتوى الصوتي",
          "نشرة بريدية خاصة بالأعضاء",
        ]),
    ];

    return of(data);
  }
}
