import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IndexComponent } from './index/index.component';
import { SubscriptionsResolver } from './subscriptions.resolver';
import { SubscriptionItemComponent } from './subscription-item/subscription-item.component';
import { RouterModule, Routes } from '@angular/router';
import { TranslatePipe } from '../shared/translate.pipe';
import { SharedModule } from '../shared/shared.module';

const ROUTES: Routes = [
  {
    path: '',
    component: IndexComponent,
    resolve: {
      items: SubscriptionsResolver
    }
  },
];

@NgModule({
  declarations: [IndexComponent, SubscriptionItemComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    SharedModule
  ],
  providers: [
  ]
})
export class SubscriptionsModule { }
