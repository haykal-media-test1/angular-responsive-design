import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from '../models/subscription';

@Component({
  selector: 'app-subscription-item',
  templateUrl: './subscription-item.component.html',
  styleUrls: ['./subscription-item.component.scss']
})
export class SubscriptionItemComponent implements OnInit {

  @Input()
  public item: Subscription;

  mode: 'max' | 'min';

  constructor() {
    this.mode = 'max';
  }

  ngOnInit(): void {
  }

  public toggleFeaturesSize(): void {
    this.mode = this.mode === 'max' ? 'min' : 'max';
  }


  public features(): string[] {
    if(this.mode === 'min')
    {
      return this.item.features.slice(0, 2);
    }
    else
    {
      return Array.from(this.item.features);
    }
  }
}
