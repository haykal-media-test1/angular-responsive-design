export const LANGUAGE = {
    subscriptions: {
        title: "الاشتراكات",
        digital: {
            title: "الاشتراكات الرقمية",
            subscribe: "اشترك الآن"
        },
        seemore: "عرض المزيد",
        seeless: "عرض أقل"
    },
    footer: {
        aboutUs: "معلومات حولنا",
        policies: "سياسات الاشتراك",
        support: "الدعم الفني",
        copyrights: "جميع الحقوق محفوظة لشركة هارفارد بزنس ريفيو",
        contactUs: "تواصل معنا",
        download: "حمل تطبيق هارفارد بزنس ريفيو"
    }
};