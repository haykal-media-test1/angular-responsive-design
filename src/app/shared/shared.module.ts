import { NgModule } from '@angular/core';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { TranslationService } from './translation.service';
import { TranslatePipe } from './translate.pipe';

@NgModule({
  declarations: [
    FooterComponent,
    HeaderComponent,
    TranslatePipe
  ],
  imports: [
  ],
  exports: [
    FooterComponent,
    HeaderComponent,
    TranslatePipe,
  ],
  providers: [
    TranslationService,
  ],
})
export class SharedModule { }
