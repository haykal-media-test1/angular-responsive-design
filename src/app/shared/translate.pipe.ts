import { Pipe, PipeTransform } from '@angular/core';
import { TranslationService } from './translation.service';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  constructor(private trans: TranslationService) {
  }

  public transform(name: string): string {
    return this.trans.translate(name);
  }

}
