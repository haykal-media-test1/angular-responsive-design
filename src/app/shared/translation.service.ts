import { Injectable } from '@angular/core';
import { LANGUAGE } from '../constants/langs/ar';

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  private lang: any;

  constructor() {
      this.lang = LANGUAGE;
  }


  public translate(name: string): string {

    try {
      let value = this.lang;
      name.split('.').forEach(e => value = value[e]);
  
      return value;
    } catch (e) {
      return name;      
    }
  }
}
